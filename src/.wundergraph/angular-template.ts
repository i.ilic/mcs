/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import {
  BaseTypeScriptDataModel,
  Template,
  TemplateOutputFile,
  templates,
  TypeScriptClient,
  TypeScriptInputModels,
  TypeScriptResponseDataModels,
  TypeScriptResponseModels,
} from '@wundergraph/sdk';
import { compile } from 'handlebars';

/**
 * Generates the Wundergraph client for Angular,
 * with ability to transfer state in SSR.
 */
export class AngularTemplate implements Template {
  async generate(): Promise<TemplateOutputFile[]> {
    const tmpl = compile(handlebarTemplate);
    const content = tmpl({});

    return Promise.resolve([
      {
        path: 'angular.ts',
        content,
        doNotEditHeader: true,
      },
    ]);
  }

  dependencies(): Template[] {
    return [
      new TypeScriptClient(),
      new TypeScriptInputModels(),
      new TypeScriptResponseModels(),
      new TypeScriptResponseDataModels(),
      new BaseTypeScriptDataModel(),
      templates.typescript.testing,
    ];
  }
}

const handlebarTemplate = `import { isPlatformServer } from '@angular/common';
import {
  inject,
  InjectionToken,
  PLATFORM_ID,
  Provider,
  Type,
} from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import {
  Client,
  CreateClientConfig,
  MutationRequestOptions,
  OperationRequestOptions,
  QueryRequestOptions,
  SubscriptionEventHandler,
  SubscriptionRequestOptions,
} from '@wundergraph/sdk/client';
import { from } from 'rxjs';
import {
  defaultClientConfig,
  Mutations,
  operationMetadata,
  Queries,
  Subscriptions,
  WunderGraphClient,
} from './client';

/**
 * ### Token for WunderGraph Angular client
 *
 * Inject this token to use the WunderGraph client in Angular.
 *
 * \`\`\`
 * class Foo {
 *   client = inject(WUNDERGRAPH);
 *
 *   getBar(): Observable<Bar> {
 *     return this.client.query$({
 *       operationName: 'GetBar'
 *     });
 *   }
 * }
 * \`\`\`
 */
export const WUNDERGRAPH = new InjectionToken<AngularWunderGraphClient>(
  'WUNDERGRAPH'
);

/**
 * ### WunderGraph Angular client
 *
 * - works with Angular Universal (SSR)
 * - has interceptors
 * - can return RxJS observables
 */
export class AngularWunderGraphClient extends WunderGraphClient {
  /**
   * Wraps the query()'s result into an RxJS observable
   */
  query$<OperationName extends keyof Queries>(
    options: OperationName extends string
      ? QueryRequestOptions<OperationName, Queries[OperationName]['input']>
      : OperationRequestOptions
  ) {
    return from(
      this.query<OperationName, Queries[OperationName]['input']>(options)
    );
  }

  /**
   * Wraps the mutate()'s result into an RxJS observable
   */
  mutate$<OperationName extends keyof Mutations>(
    options: OperationName extends string
      ? MutationRequestOptions<OperationName, Mutations[OperationName]['input']>
      : OperationRequestOptions
  ) {
    return from(
      this.mutate<
        OperationName,
        Mutations[OperationName]['input'],
        Mutations[OperationName]['response']
      >(options)
    );
  }

  /**
   * Wraps the subscribe()'s result into an RxJS observable
   */
  subscribe$<OperationName extends keyof Subscriptions>(
    options: OperationName extends string
      ? SubscriptionRequestOptions<
          OperationName,
          Subscriptions[OperationName]['input']
        >
      : SubscriptionRequestOptions,
    cb?: SubscriptionEventHandler<
      Subscriptions[OperationName]['response']['data'],
      Subscriptions[OperationName]['response']['error']
    >
  ) {
    return from(
      this.subscribe<OperationName, Subscriptions[OperationName]['input']>(
        options,
        cb
      )
    );
  }
}

/**
 * ### WunderGraph provider function
 *
 * Provides the WunderGraph client as an injection token.
 *
 * @see {@link WUNDERGRAPH} token for further info.
 */
export function provideWunderGraph(config?: CreateClientConfig): Provider[] {
  return provideWunderGraphFactory(() => config);
}

/**
 * ### WunderGraph provider factory function
 *
 * Provides the WunderGraph client as an injection token.
 *
 * @see {@link WUNDERGRAPH} token for further info.
 */
export function provideWunderGraphFactory(
  factoryFn: () => CreateClientConfig | undefined
): Provider[] {
  return [
    {
      provide: WUNDERGRAPH,
      useFactory: () => {
        const config = factoryFn();
        const client = new AngularWunderGraphClient({
          ...defaultClientConfig,
          ...config,
          operationMetadata,
          csrfEnabled: false,
          customFetch: createCustomFetch(),
        });

        return decorateClient(client);
      },
    },
    {
      provide: Client,
      useExisting: WUNDERGRAPH,
    },
  ];
}

/**
 * Enhances the client by adding the transfer state feature.
 * The mutation is not enhanced, only the query method.
 */
function decorateClient(client: WunderGraphClient) {
  const isServer = isPlatformServer(inject(PLATFORM_ID));

  return isServer
    ? forceZoneToWaitForQuery(transferQueryData(client))
    : useTransferredQueryData(client);
}

/**
 * Returns the transferred state, or calls query method.
 */
function useTransferredQueryData(client: WunderGraphClient) {
  const state = inject(TransferState, { optional: true });
  const query = client.query;

  client.query = (...params: Parameters<typeof query>) => {
    const key = createStateKeyFromQuery(params);

    const transferredData = state?.get(key, null);
    state?.remove(key);

    return transferredData
      ? Promise.resolve(transferredData)
      : query.apply(client, params);
  };

  return client;
}

/**
 * Sets the transfer state.
 */
function transferQueryData(client: WunderGraphClient) {
  const state = inject(TransferState, { optional: true });
  const query = client.query;

  client.query = (...params: Parameters<typeof query>) => {
    const key = createStateKeyFromQuery(params);

    return query.apply(client, params).then((data) => {
      state?.set(key, data);
      return data;
    });
  };

  return client;
}

/**
 * Creates a state key for a WunderGraph operation.
 */
function createStateKeyFromQuery(params: [QueryRequestOptions]) {
  const { operationName, input } = params[0];

  if (input) {
    const hash = generateHash(JSON.stringify(input));
    return makeStateKey<unknown>(\`\${operationName}-\${hash}\`);
  }

  return makeStateKey<unknown>(operationName);
}

/**
 * A simple hash generator.
 */
export function generateHash(value: string): string {
  let last = value.length;
  let result = 0;

  while (last--) {
    let charCode = value.charCodeAt(last);
    result = (result << 9) + charCode - result;
    result >>>= 0;
  }

  return result.toString(16).replace(/^([^-])/, '+$1');
}

/**
 * Since Angular is unable to wait for async tasks in SSR mode,
 * it uses a trick with Zone.js - see more below.
 */
function forceZoneToWaitForQuery(client: WunderGraphClient) {
  const query = client.query;

  client.query = (...params: Parameters<typeof query>) => {
    const finalize = createBackgroundMacroTask();

    return query.apply(client, params).finally(finalize);
  };

  return client;
}

/**
 * Angular's workaround for intercepting the HTTP calls.
 * See more at: https://github.com/angular/angular/blob/294fae02eb32da11202ed4c1cc26ee5a8c291afc/packages/common/http/src/xhr.ts#L354-L358
 */
function createBackgroundMacroTask(): VoidFunction {
  const timeout = setTimeout(() => void 0, MAX_INT);

  return () => clearTimeout(timeout);
}

const MAX_INT = 2147483647;

/**
 * ### Utility function to provide multiple interceptors
 *
 * **Important:** Interceptors should be injectable and already provided. You may use \`providedIn: 'root'\` for that. → https://angular.io/guide/singleton-services#using-providedin
 */
export function provideFetchInterceptors(
  interceptors: Type<FetchInterceptor>[]
): Provider[] {
  return interceptors.map((interceptor) => ({
    provide: FETCH_INTERCEPTORS,
    multi: true,
    useExisting: interceptor,
  }));
}

/**
 * **Internal:** Use \`provideFetchInterceptors\` to provide interceptors.
 *
 * This token is intended to be used with \`multi: true\`.
 */
const FETCH_INTERCEPTORS = new InjectionToken<FetchInterceptor[]>(
  'FETCH_INTERCEPTORS'
);

/**
 * ### Base class for fetch interceptors
 *
 * **Important:** Do not forget to make your interceptors injectable and provide them explicitly.
 *
 * @see {@link provideFetchInterceptors}
 */
export abstract class FetchInterceptor {
  abstract handle(request: Request, next: FetchHandlerFn): Promise<Response>;
}

/**
 * ### Type of chained fetch handlers
 *
 * This is a utility type to use in interceptors.
 */
export type FetchHandlerFn = (request: Request) => Promise<Response>;

/**
 * Called by \`provideWunderGraph\` to create a client with interceptors.
 */
function createCustomFetch() {
  const interceptors = inject(FETCH_INTERCEPTORS, { optional: true }) ?? [];

  return (input: RequestInfo, init?: RequestInit): Promise<Response> => {
    const dedupedHandlers = new Set(interceptors);
    dedupedHandlers.add(new FetchHandler());

    const handleRequest = Array.from(dedupedHandlers).reduceRight(
      (acc, handler) => (req: Request) => handler.handle(req, acc),
      (_req: Request) => Promise.resolve(new Response())
    );

    return handleRequest(new Request(input, init));
  };
}

/**
 * **Internal:** This is called as the last handler in the chain.
 */
class FetchHandler extends FetchInterceptor {
  handle(request: Request) {
    return fetch(request);
  }
}
`;
