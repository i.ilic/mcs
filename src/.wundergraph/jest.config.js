/** @type {import("jest").Config} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  setupFilesAfterEnv: ["<rootDir>/test-setup.ts"],
  moduleFileExtensions: ["ts", "js", "mts", "mjs"],
  coverageDirectory: "<rootDir>/../../coverage/wundergraph",
  coverageReporters: ["html"],
  collectCoverageFrom: [
    "<rootDir>/operations/**/*.ts",
    "<rootDir>/utils/**/*.ts",
  ],
  coveragePathIgnorePatterns: [],
  transform: {
    "^.+\\.m?[tj]s$": [
      "ts-jest",
      {
        tsconfig: "<rootDir>/tests/tsconfig.json",
      },
    ],
  },
  transformIgnorePatterns: ["node_modules/(?!.*\\.mjs$)"],
};
