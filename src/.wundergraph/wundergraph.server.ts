import { configureWunderGraphServer } from '@wundergraph/sdk/server';

class BffCustomContext {
  occURL = process.env.OCC_URL;
  storefrontURL = process.env.STOREFRONT_URL;
}

export class GlobalContext extends BffCustomContext {
  async cleanup() {
    // DO ANY CLEANUP HERE.
  }
}

export class RequestContext extends BffCustomContext {
  constructor(public readonly global: GlobalContext) {
    super();
  }

  async cleanup() {
    // DO ANY CLEANUP HERE.
  }
}

declare module '@wundergraph/sdk/server' {
  export interface CustomContext {
    request: GlobalContext;
    global: GlobalContext;
  }
}

export default configureWunderGraphServer(() => ({
  context: {
    global: {
      create: async () => {
        return new GlobalContext();
      },
      release: async (globalContext) => {
        await globalContext.cleanup(); // called before server shutdown
      },
    },
    request: {
      create: async (globalContext) => {
        return new RequestContext(globalContext);
      },
      release: async (requestContext) => {
        await requestContext.cleanup(); // called after every request
      },
    },
  },
  hooks: {
    global: {},
    queries: {},
    mutations: {},
  },
}));

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace NodeJS {
    interface ProcessEnv {
      STOREFRONT_URL: string;
      OCC_URL: string;
      OCC_SCHEMA_URL: string;
      AUTH_URL: string;
      API_DISCOVERY_URL: string;
      API_DISCOVERY_CLIENT_ID: string;
      API_DISCOVERY_CLIENT_SECRET: string;
      API_DISCOVERY_GRANT_TYPE: string;
      ALLOWED_ORIGIN: string;
    }
  }
}
