export class SmartResponseError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'SmartResponseError';
  }
}
