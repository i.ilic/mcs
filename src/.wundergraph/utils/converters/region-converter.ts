import type { Region } from '@spartacus/core';

export function convertRegion(
  region?: Region & { codeShort?: string; code?: string; country?: string }
) {
  if (!region) return;

  const { code, codeShort, country, ...regionWithoutIsoCode } = region;

  return {
    ...regionWithoutIsoCode,
    isocodeShort: codeShort,
    isocode: code,
    countryIso: country,
  };
}
