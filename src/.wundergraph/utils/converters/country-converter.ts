import type { Country } from '@spartacus/core';

export function convertCountry(country: Country & { code: string }) {
  const { code, ...countryWithoutIsoCode } = country;

  return {
    ...countryWithoutIsoCode,
    isocode: code,
  };
}
