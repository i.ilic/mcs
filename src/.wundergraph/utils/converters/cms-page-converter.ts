import type {
  CmsStructureModel,
  ContentSlotComponentData,
  ContentSlotData,
  Occ,
  Page,
} from '@spartacus/core';
import {
  OccGetPageByIdAndUserResponseData,
  OccGetPageWithUserResponseData,
} from '../../generated/models';

export function convertCmsPageResponse(
  response:
    | OccGetPageWithUserResponseData['occ_getPageWithUser']
    | OccGetPageByIdAndUserResponseData['occ_getPageByIdAndUser']
) {
  const source = response ?? {};

  Object.assign(source, source?.additionalProperties ?? {});

  const target: CmsStructureModel = {};

  normalizePageData(source as Occ.CMSPage, target);
  normalizePageSlotData(source as Occ.CMSPage, target);
  normalizePageComponentData(source as Occ.CMSPage, target);
  normalizeComponentData(source as Occ.CMSPage, target);

  return target;
}

/**
 * Converts the OCC cms page model to the `Page` in the `CmsStructureModel`.
 */
function normalizePageData(source: Occ.CMSPage, target: CmsStructureModel) {
  if (!source) {
    return;
  }
  const page: Page = {};

  if (source.name) {
    page.name = source.name;
  }
  if (source.typeCode) {
    page.type = source.typeCode;
  }
  if (source.label) {
    page.label = source.label;
  }
  if (source.template) {
    page.template = source.template;
  }
  if (source.uid) {
    page.pageId = source.uid;
  }
  if (source.title) {
    page.title = source.title;
  }
  if (source.description) {
    page.description = source.description;
  }
  if (source.properties) {
    page.properties = source.properties;
  }

  normalizeRobots(source, page);

  target.page = page;
}

/**
 * Adds a ContentSlotData for each page slot in the `CmsStructureModel`.
 */
function normalizePageSlotData(source: Occ.CMSPage, target: CmsStructureModel) {
  if (!source?.contentSlots) {
    return;
  }
  if (
    source.contentSlots.contentSlot &&
    !Array.isArray(source.contentSlots.contentSlot)
  ) {
    source.contentSlots.contentSlot = [source.contentSlots.contentSlot];
  }
  target.page = target.page ?? {};
  target.page.slots = {};
  for (const slot of source.contentSlots.contentSlot ?? []) {
    if (slot.position) {
      target.page.slots[slot.position] = {} as ContentSlotData;
      if (slot.properties) {
        target.page.slots[slot.position].properties = slot.properties;
      }
    }
  }
}

/**
 * Registers the `ContentSlotComponentData` for each component.
 */
function normalizePageComponentData(
  source: Occ.CMSPage,
  target: CmsStructureModel
) {
  if (!source?.contentSlots?.contentSlot) {
    return;
  }
  for (const slot of source.contentSlots.contentSlot) {
    if (Array.isArray(slot.components?.component)) {
      for (const component of slot.components?.component ?? []) {
        const comp: ContentSlotComponentData = {
          uid: component.uid,
          typeCode: component.typeCode,
        };
        if (component.properties) {
          comp.properties = component.properties;
        }

        comp.flexType = getFlexTypeFromComponent(component);

        if (slot.position) {
          const targetSlot = target.page?.slots?.[slot.position];
          if (targetSlot) {
            if (!targetSlot.components) {
              targetSlot.components = [];
            }
            targetSlot.components.push(comp);
          }
        }
      }
    }
  }
}

/**
 * Returns the flex type based on the configuration of component properties
 */
function getFlexTypeFromComponent(component: Occ.Component | any) {
  if (component.typeCode === CMS_FLEX_COMPONENT_TYPE) {
    return component.flexType;
  } else if (component.typeCode === JSP_INCLUDE_CMS_COMPONENT_TYPE) {
    return component.uid;
  }
  return component.typeCode;
}

/**
 * Adds the actual component data whenever available in the CMS page data.
 *
 * If the data is not populated in this payload, it is loaded separately
 * (`OccCmsComponentAdapter`).
 */
function normalizeComponentData(
  source: Occ.CMSPage,
  target: CmsStructureModel
) {
  if (!source?.contentSlots?.contentSlot) {
    return;
  }

  for (const slot of source.contentSlots.contentSlot) {
    for (const component of slot.components?.component ?? []) {
      // while we're hoping to get this right from the backend api,
      // the OCC api stills seems out of sync with the right model.
      if (component.modifiedtime) {
        component.modifiedTime = component.modifiedtime;
        delete component.modifiedtime;
      }

      // we don't put properties into component state
      if (component.properties) {
        component.properties = undefined;
      }
      if (!target.components) {
        target.components = [];
      }
      target.components.push(component);
    }
  }
}

/**
 * Normalizes the page robot string to an array of `PageRobotsMeta` items.
 */
function normalizeRobots(source: Occ.CMSPage, target: Page): void {
  const robots = [];

  if (source.robotTag) {
    switch (source.robotTag) {
      case PageRobots.INDEX_FOLLOW:
        robots.push(PageRobotsMeta.INDEX);
        robots.push(PageRobotsMeta.FOLLOW);
        break;
      case PageRobots.NOINDEX_FOLLOW:
        robots.push(PageRobotsMeta.NOINDEX);
        robots.push(PageRobotsMeta.FOLLOW);
        break;
      case PageRobots.INDEX_NOFOLLOW:
        robots.push(PageRobotsMeta.INDEX);
        robots.push(PageRobotsMeta.NOFOLLOW);
        break;
      case PageRobots.NOINDEX_NOFOLLOW:
        robots.push(PageRobotsMeta.NOINDEX);
        robots.push(PageRobotsMeta.NOFOLLOW);
        break;
    }
  }

  target.robots = robots;
}

const JSP_INCLUDE_CMS_COMPONENT_TYPE = 'JspIncludeComponent';
const CMS_FLEX_COMPONENT_TYPE = 'CMSFlexComponent';

enum PageRobots {
  INDEX_FOLLOW = 'INDEX_FOLLOW',
  NOINDEX_FOLLOW = 'NOINDEX_FOLLOW',
  INDEX_NOFOLLOW = 'INDEX_NOFOLLOW',
  NOINDEX_NOFOLLOW = 'NOINDEX_NOFOLLOW',
}

enum PageRobotsMeta {
  INDEX = 'INDEX',
  NOINDEX = 'NOINDEX',
  FOLLOW = 'FOLLOW',
  NOFOLLOW = 'NOFOLLOW',
}
