import { z } from 'zod';

export const fields = z.union([
  z.literal('BASIC'),
  z.literal('DEFAULT'),
  z.literal('FULL'),
]);
