export function formatPrice(options: {
  locale: string;
  currency?: string;
  price: number;
}) {
  const { locale, currency, price } = options;

  const priceFormatter = new Intl.NumberFormat(locale, {
    style: 'currency',
    currency,
    useGrouping: false,
  });

  return priceFormatter.format(price);
}
