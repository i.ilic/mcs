import {
  discoverMCS,
  introspectDiscovery,
  introspectOCC,
} from '@vivaldi/bff-utils';
import {
  configureWunderGraphApplication,
  cors,
  EnvironmentVariable,
} from '@wundergraph/sdk';
import generate from './wundergraph.generate';
import operations from './wundergraph.operations';
import server from './wundergraph.server';

const occ = introspectOCC();

const mcsStorefrontCheckoutV1 = introspectDiscovery(
  discoverMCS(),
  (discovery) => discovery.storefront.checkout.v1()
);

configureWunderGraphApplication({
  apis: [occ, mcsStorefrontCheckoutV1],
  server,
  generate,
  operations,
  security: {
    enableGraphQLEndpoint:
      process.env['NODE_ENV'] !== 'production' ||
      process.env['GITPOD_WORKSPACE_ID'] !== undefined,
  },
  experimental: {
    orm: false,
  },
  cors: {
    ...cors.allowAll,
    allowedOrigins: [new EnvironmentVariable('ALLOWED_ORIGIN')],
  },
});
