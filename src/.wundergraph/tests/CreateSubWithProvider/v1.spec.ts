import * as msw from 'msw';
import { setupServer } from 'msw/node';

import type { SmartCreateSubWithProviderV1ResponseData } from '../../generated/models';

const mockResponse = `
html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title>Mocked Silent Order Post Page</title>
    <link
      rel="shortcut icon"
      href="/acceleratorservices/_ui/hop-mock/images/favicon.ico"
      type="image/x-icon"
    />
    <link
      rel="stylesheet"
      type="text/css"
      media="screen"
      href="/acceleratorservices/_ui/hop-mock/css/common.css"
    />
  </head>
  <body onload="document.silentOrderPostForm.submit()">
    <div id="mockwrapper">
      <div id="mockpage">
        <div id="mockHeader">
          <div class="logo">
            <img
              alt="logo"
              src="/acceleratorservices/_ui/hop-mock/images/logo.png"
            />
          </div>
        </div>
        <div style="clear: both"></div>
        <div id="item_container_holder">
          <div class="item_container">
            <div id="debugWelcome">
              <h3>
                <img
                  src="/acceleratorservices/_ui/hop-mock/images/spinner.gif"
                  alt="image spinner"
                />&nbsp; Please wait while we transfer you
              </h3>
            </div>
          </div>
          <div class="item_container">
            <form
              id="silentOrderPostForm"
              name="silentOrderPostForm"
              action="javascript:false;"
              method="post"
            >
              <div id="postFormItems">
                <dl>
                  <input
                    type="hidden"
                    id="card_expirationYear"
                    name="card_expirationYear"
                    value="2029"
                  />
                </dl>
              </div>
            </form>
          </div>
        </div>
        <div style="clear: both"></div>
        <div id="footer"></div>
      </div>
    </div>
  </body>
</html>
`;

const expectedResult: SmartCreateSubWithProviderV1ResponseData = {
  card_expirationYear: '2030',
  defaultPayment: true,
  savePaymentInfo: true,
};

describe.skip('CreateSubWithProvider', () => {
  it('should return the proper response', async () => {
    const server = setupServer();

    const postUrl = 'http://localhost:8080/responseurl';
    const input = {
      postUrl,
      parameters: {
        card_expirationYear: 2030,
      },
    };

    server.use(
      msw.http.post(postUrl, async ({ request }) => {
        const body = await request.json();

        expect(body).toEqual(input);

        return new msw.HttpResponse(mockResponse);
      })
    );

    const result = await wgTs.testServer.client().mutate({
      operationName: 'smart/CreateSubWithProvider/v1',
      input,
    });

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
