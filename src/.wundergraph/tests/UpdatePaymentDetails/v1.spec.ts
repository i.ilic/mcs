import type { OccUpdatePaymentDetailsResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const paymentDetailsId = '000000';

describe('UpdatePaymentDetails', () => {
  const mockResponse: OccUpdatePaymentDetailsResponseData['occ_updatePaymentDetails'] =
    {};

  const expectedResult = {
    errors: null,
  };

  it('should return proper response', async () => {
    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'PATCH' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/paymentdetails/${paymentDetailsId}`,
      handler: async () => ({ status: 200, body: mockResponse }),
    });

    const result = await wgTs.testServer.client().mutate({
      operationName: 'smart/UpdatePaymentDetails/v1',
      input: {
        baseSiteId,
        userId,
        paymentDetailsId,
        input: { accountHolderName: 'foo' },
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
