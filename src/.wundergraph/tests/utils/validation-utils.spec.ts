import { fields } from '../../utils/validation-utils';

describe('fields', () => {
  it('should successfully parse valid values', () => {
    expect(fields.parse('BASIC')).toBe('BASIC');
    expect(fields.parse('DEFAULT')).toBe('DEFAULT');
    expect(fields.parse('FULL')).toBe('FULL');
  });

  it('should throw when parsing invalid values', () => {
    expect(() => fields.parse('FOO')).toThrow();
    expect(() => fields.parse('BAR')).toThrow();
  });
});
