import { formatPrice } from '../../utils/price-formatter-utils';

describe('formatPrices', () => {
  it('should format the prices', async () => {
    const price = 10;
    const result = formatPrice({
      locale: 'en',
      currency: 'USD',
      price,
    });

    expect(result).toEqual(`$${price}.00`);
  });
});
