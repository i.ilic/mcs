import type { Region } from '@spartacus/core';
import { convertRegion } from '../../../utils/converters/region-converter';

describe('convertRegion', () => {
  it('should return undefined when no region is given', () => {
    const result = convertRegion();
    expect(result).toBeUndefined();
  });

  it('should convert the given region', async () => {
    const expectedRegion: Region = {
      isocode: 'QC',
      isocodeShort: 'QC',
      countryIso: 'CA',
      name: 'Canada',
    };
    const inputRegion = {
      ...expectedRegion,
      code: 'QC',
      codeShort: 'QC',
      country: 'CA',
    };

    const result = convertRegion(inputRegion);
    expect(result).toEqual(expectedRegion);
  });
});
