import type { CmsStructureModel } from '@spartacus/core';
import type { OccGetPageWithUserResponseData } from '../../../generated/models';
import { convertCmsPageResponse } from '../../../utils/converters/cms-page-converter';

const mockPageName = 'page name';
const mockSlotName = 'slot name';
const mockSlotPosition = 'top position';
const mockUid = 'mock uid';
const mockComponents = [
  {
    uid: mockUid,
    name: 'baz',
    flexType: 'SomeFlexComponentCmsName',
    typeCode: 'CMSFlexComponent',
  },
];

const mockResponse: OccGetPageWithUserResponseData['occ_getPageWithUser'] = {
  name: mockPageName,
  additionalProperties: {
    contentSlots: {
      contentSlot: [
        {
          name: mockSlotName,
          position: mockSlotPosition,
          components: {
            component: mockComponents,
          },
        },
      ],
    },
  },
};

const expectedResult: CmsStructureModel = {
  components: mockComponents,
  page: {
    name: mockPageName,
    robots: [],
    slots: {
      [mockSlotPosition]: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        components: mockComponents.map(({ name, ...rest }) => ({
          ...rest,
        })),
      },
    },
  },
};

describe('cms page converter', () => {
  it('should convert the given data', async () => {
    const result = convertCmsPageResponse(mockResponse);
    expect(result).toEqual(expectedResult);
  });
});
