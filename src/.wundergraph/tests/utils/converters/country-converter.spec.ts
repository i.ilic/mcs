import type { Country } from '@spartacus/core';
import { convertCountry } from '../../../utils/converters/country-converter';

describe('convertCountry', () => {
  it('should convert the given country', async () => {
    const expectedCountry: Country = {
      isocode: 'CA',
      name: 'Canada',
    };
    const inputCountry = {
      ...expectedCountry,
      code: 'CA',
    };

    const result = convertCountry(inputCountry);
    expect(result).toEqual(expectedCountry);
  });
});
