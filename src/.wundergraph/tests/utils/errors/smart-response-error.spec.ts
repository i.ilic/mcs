import { SmartResponseError } from '../../../utils/errors/smart-response-error';

describe('SmartResponseError', () => {
  it('should be an instance of SmartResponseError', () => {
    const error = new SmartResponseError('foo');
    expect(error).toBeInstanceOf(SmartResponseError);
    expect(error.name).toEqual('SmartResponseError');
  });
});
