import type { CmsStructureModel } from '@spartacus/core';
import type { OccGetPageWithUserResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const fields = 'DEFAULT';

const mockPageName = 'page name';
const mockSlotName = 'slot name';
const mockSlotPosition = 'top position';
const mockUid = 'mock uid';
const mockComponents = [
  {
    uid: mockUid,
    name: 'baz',
  },
];
const mockResponse: OccGetPageWithUserResponseData['occ_getPageWithUser'] = {
  additionalProperties: {
    components: {
      component: mockComponents,
    },
  },
  contentSlots: {
    contentSlot: [
      {
        name: mockSlotName,
        position: mockSlotPosition,
        components: {
          component: mockComponents,
        },
      },
    ],
  },
  name: mockPageName,
};

const expectedResult: CmsStructureModel = {
  components: mockComponents,
  page: {
    name: mockPageName,
    robots: [],
    slots: {
      [mockSlotPosition]: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        components: mockComponents.map(({ name, ...rest }) => ({
          ...rest,
        })),
      },
    },
  },
};

describe('GetPageWithUser', () => {
  describe(`when pageContext's type is NOT given`, () => {
    describe(`and when pageContext's id is productList and when cmsTicketId is NOT given`, () => {
      it('should call occ/GetPageByIdAndUser with the proper input', async () => {
        const pageContext = {
          id: 'productList',
        };
        const routePageContextId = '576';

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages/${pageContext.id}?categoryCode=${routePageContextId}&fields=${fields}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId,
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });

    describe(`and when pageContext's id is NOT productList`, () => {
      it('should call occ/GetPageByIdAndUser with the proper input', async () => {
        const pageContext = {
          id: 'foo',
        };

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages/${pageContext.id}?fields=${fields}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId: 'foo',
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });

    describe('and when cmsTicketId is given', () => {
      it('should call occ/GetPageByIdAndUser with the proper input', async () => {
        const pageContext = {
          id: 'productList',
        };
        const routePageContextId = '576';
        const cmsTicketId = '1234-5678';

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages/${pageContext.id}?categoryCode=${routePageContextId}&fields=${fields}&cmsTicketId=${cmsTicketId}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId,
            cmsTicketId,
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });
  });

  describe(`when pageContext's type is present`, () => {
    describe(`and when pageContext id is homepage`, () => {
      it('should call occ/GetPageWithUser with proper input', async () => {
        const pageContext = {
          id: '__HOMEPAGE__',
          type: 'ContentPage' as const,
        };

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages?fields=${fields}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId: 'foo',
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });
    describe(`and when pageContext id is smartedit`, () => {
      it('should call occ/GetPageWithUser with proper input', async () => {
        const pageContext = {
          id: 'smartedit-preview',
          type: 'ContentPage' as const,
        };

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages?fields=${fields}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId: 'foo',
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });

    describe(`and when pageContext id is ContentPage`, () => {
      it('should call occ/GetPageWithUser with proper input', async () => {
        const pageContext = {
          id: '/contact',
          type: 'ContentPage' as const,
        };

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages?fields=${fields}&pageLabelOrId=${encodeURIComponent(
                pageContext.id
              )}&pageType=${pageContext.type}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId: 'foo',
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });

    describe(`and when pageContext id is NOT ContentPage`, () => {
      it('should call occ/GetPageWithUser with proper input', async () => {
        const pageContext = {
          id: '576',
          type: 'CategoryPage' as const,
        };

        const scope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/${baseSiteId}/users/${userId}/cms/pages?code=${pageContext.id}&fields=${fields}&pageType=${pageContext.type}`,
          handler: async () => ({ status: 200, body: mockResponse }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetPageWithUser/v1',
          input: {
            baseSiteId,
            userId,
            fields,
            pageContext,
            routePageContextId: 'foo',
          },
        });

        scope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });
  });
});
