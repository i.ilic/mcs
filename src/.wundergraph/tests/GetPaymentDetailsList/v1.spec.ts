import type { OccGetPaymentDetailsListResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const saved = true;

describe('GetPaymentDetailsList', () => {
  const mockResponse: OccGetPaymentDetailsListResponseData['occ_getPaymentDetailsList'] =
    {
      payments: [
        {
          accountHolderName: 'foo',
        },
      ],
    };

  const expectedResult = mockResponse.payments?.map((payment) => ({
    ...payment,
    billingAddress: null,
    cardNumber: null,
    cardType: null,
    defaultPayment: null,
    expiryMonth: null,
    expiryYear: null,
    id: null,
    issueNumber: null,
    saved: null,
    startMonth: null,
    startYear: null,
    subscriptionId: null,
  }));

  it('should return proper response', async () => {
    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/paymentdetails?fields=DEFAULT&saved=${saved}`,
      handler: async () => ({ status: 200, body: mockResponse }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetPaymentDetailsList/v1',
      input: {
        baseSiteId,
        userId,
        saved,
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
