import type { OccReplaceCartPaymentDetailsResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const cartId = '0000001';
const paymentDetailsId = '123456';

const mockResponse: OccReplaceCartPaymentDetailsResponseData['occ_replaceCartPaymentDetails'] =
  {};

const expectedResult = {
  ...mockResponse,
  errors: null,
};

describe('ReplaceCartPaymentDetails', () => {
  it('should return the proper response', async () => {
    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'PUT' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${cartId}/paymentdetails?paymentDetailsId=${paymentDetailsId}`,
      handler: async () => ({
        status: 200,
        body: mockResponse,
      }),
    });

    const result = await wgTs.testServer.client().mutate({
      operationName: 'smart/ReplaceCartPaymentDetails/v1',
      input: {
        baseSiteId,
        userId,
        cartId,
        paymentDetailsId,
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
