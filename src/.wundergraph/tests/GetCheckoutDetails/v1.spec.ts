import type {
  McsStorefrontCheckoutV1GetCheckoutResponseData,
  OccGetCartResponseData,
} from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const checkoutId = '0000001';
const locale = 'en';

type GetCheckoutResponse = NonNullable<
  McsStorefrontCheckoutV1GetCheckoutResponseData['mcsStorefrontCheckoutV1_getCheckout']
>;

const shippingAddressMock: GetCheckoutResponse['shippingAddress'] = {
  titleCode: 'foo',
  firstName: 'foo',
  lastName: 'foo',
  country: { code: 'foo' },
  addressLine1: 'foo',
  postalCode: 'foo',
  town: 'foo',
};
const expectedDeliveryAddress = {
  ...shippingAddressMock,
  country: {
    isocode: shippingAddressMock.country.code,
    name: null,
  },
  companyName: null,
  district: null,
  id: null,
  line1: shippingAddressMock.addressLine1,
  line2: null,
  phone: null,
  title: null,
  addressLine2: null,
};

const deliveryOptionMock: GetCheckoutResponse['deliveryOption'] = {
  code: 'premium',
  deliveryCost: 10,
};
const expectedDeliveryOptions = {
  ...deliveryOptionMock,
  deliveryCost: {
    formattedValue: '$10.00',
  },
  description: null,
  name: null,
};

const paymentInfoMock: NonNullable<
  OccGetCartResponseData['occ_getCart']
>['paymentInfo'] = {
  cardNumber: '1234123412341234',
  billingAddress: {
    firstName: 'bar',
    lastName: 'bar',
    line1: 'bar',
    postalCode: 'bar',
    country: { isocode: 'bar' },
    titleCode: 'bar',
    town: 'bar',
  },
};
const expectedPaymentInfo = {
  ...paymentInfoMock,
  accountHolderName: null,
  billingAddress: {
    ...paymentInfoMock.billingAddress,
    cellphone: null,
    companyName: null,
    country: {
      ...paymentInfoMock.billingAddress?.country,
      name: null,
    },
    defaultAddress: null,
    district: null,
    email: null,
    formattedAddress: null,
    id: null,
    line2: null,
    phone: null,
    region: null,
    shippingAddress: null,
    title: null,
    visibleInAddressBook: null,
  },
  cardNumber: '1234123412341234',
  cardType: null,
  defaultPayment: null,
  expiryMonth: null,
  expiryYear: null,
  id: null,
  issueNumber: null,
  saved: null,
  startMonth: null,
  startYear: null,
  subscriptionId: null,
};

describe('GetCheckoutDetails', () => {
  it('should convert all the data properly', async () => {
    const mockGetCheckoutResponse: GetCheckoutResponse = {
      currency: { code: 'USD' },
      shippingAddress: shippingAddressMock,
      deliveryOption: deliveryOptionMock,
    };
    const mockGetCartDetailsResponse: OccGetCartResponseData['occ_getCart'] = {
      deliveryMode: {
        deliveryCost: {
          formattedValue: '$10',
        },
      },
      paymentInfo: paymentInfoMock,
    };
    const expectedResult = {
      deliveryAddress: expectedDeliveryAddress,
      deliveryMode: expectedDeliveryOptions,
      paymentInfo: expectedPaymentInfo,
    };

    const checkoutDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}`,
      handler: async () => ({ status: 200, body: mockGetCheckoutResponse }),
    });
    const cartDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${checkoutId}?fields=paymentInfo%28FULL%29`,
      handler: async () => ({
        status: 200,
        body: mockGetCartDetailsResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetCheckoutDetails/v1',
      input: {
        baseSiteId,
        userId,
        checkoutId,
        locale,
      },
    });

    checkoutDetailsScope.done();
    cartDetailsScope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });

  it('should convert only the deliveryAddress', async () => {
    const mockGetCheckoutResponse: GetCheckoutResponse = {
      shippingAddress: shippingAddressMock,
    };
    const expectedResult = {
      deliveryAddress: expectedDeliveryAddress,
    };
    const mockGetCartDetailsResponse: OccGetCartResponseData['occ_getCart'] =
      {};

    const checkoutDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}`,
      handler: async () => ({ status: 200, body: mockGetCheckoutResponse }),
    });
    const cartDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${checkoutId}?fields=paymentInfo%28FULL%29`,
      handler: async () => ({
        status: 200,
        body: mockGetCartDetailsResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetCheckoutDetails/v1',
      input: {
        baseSiteId,
        userId,
        checkoutId,
        locale,
      },
    });

    checkoutDetailsScope.done();
    cartDetailsScope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });

  it('should convert only the deliveryOption', async () => {
    const mockGetCheckoutResponse: GetCheckoutResponse = {
      currency: { code: 'USD' },
      deliveryOption: deliveryOptionMock,
    };
    const expectedResult = {
      deliveryMode: expectedDeliveryOptions,
    };
    const mockGetCartDetailsResponse: OccGetCartResponseData['occ_getCart'] =
      {};

    const checkoutDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}`,
      handler: async () => ({ status: 200, body: mockGetCheckoutResponse }),
    });
    const cartDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${checkoutId}?fields=paymentInfo%28FULL%29`,
      handler: async () => ({
        status: 200,
        body: mockGetCartDetailsResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetCheckoutDetails/v1',
      input: {
        baseSiteId,
        userId,
        checkoutId,
        locale,
      },
    });

    checkoutDetailsScope.done();
    cartDetailsScope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });

  it('should convert only the paymentInfo', async () => {
    const mockGetCheckoutResponse: GetCheckoutResponse = {};
    const mockGetCartDetailsResponse: OccGetCartResponseData['occ_getCart'] = {
      paymentInfo: paymentInfoMock,
    };
    const expectedResult = {
      paymentInfo: expectedPaymentInfo,
    };

    const checkoutDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}`,
      handler: async () => ({ status: 200, body: mockGetCheckoutResponse }),
    });
    const cartDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${checkoutId}?fields=paymentInfo%28FULL%29`,
      handler: async () => ({
        status: 200,
        body: mockGetCartDetailsResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetCheckoutDetails/v1',
      input: {
        baseSiteId,
        userId,
        checkoutId,
        locale,
      },
    });

    checkoutDetailsScope.done();
    cartDetailsScope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
