import type { Occ } from '@spartacus/core';
import type {
  McsStorefrontCheckoutV1GetCheckoutResponseData,
  McsStorefrontCheckoutV1GetDeliveryOptionsResponseData,
} from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const checkoutId = '0000001';
const locale = 'en';

const mockGetCheckoutResponse: McsStorefrontCheckoutV1GetCheckoutResponseData['mcsStorefrontCheckoutV1_getCheckout'] =
  {
    currency: { code: 'USD' },
  };

const mockGetDeliveryOptionsResponse: McsStorefrontCheckoutV1GetDeliveryOptionsResponseData['mcsStorefrontCheckoutV1_getDeliveryOptions'] =
  {
    pagination: {
      count: 0,
      page: 0,
      pageSize: 0,
    },
    results: [
      {
        code: 'premium',
        description: 'premium',
        name: 'premium',
        deliveryCost: 10,
      },
    ],
  };

const expectedResult: Occ.DeliveryMode[] = [
  {
    ...mockGetDeliveryOptionsResponse.results?.[0],
    deliveryCost: {
      formattedValue: '$10.00',
    },
  },
];

describe('GetDeliveryOptions', () => {
  it('should convert the data', async () => {
    const checkoutDetailsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}`,
      handler: async () => ({ status: 200, body: mockGetCheckoutResponse }),
    });
    const deliveryOptionsScope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}/deliveryOptions`,
      handler: async () => ({
        status: 200,
        body: mockGetDeliveryOptionsResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetDeliveryOptions/v1',
      input: {
        baseSiteId,
        userId,
        checkoutId,
        locale,
      },
    });

    checkoutDetailsScope.done();
    deliveryOptionsScope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
