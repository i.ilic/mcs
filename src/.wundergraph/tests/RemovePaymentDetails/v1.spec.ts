import type { OccRemovePaymentDetailsResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const paymentDetailsId = '000000';

describe('RemovePaymentDetails', () => {
  const mockResponse: OccRemovePaymentDetailsResponseData['occ_removePaymentDetails'] =
    {};

  const expectedResult = {
    Void: '',
  };

  it('should return proper response', async () => {
    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'DELETE' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/paymentdetails/${paymentDetailsId}`,
      handler: async () => ({ status: 200, body: mockResponse }),
    });

    const result = await wgTs.testServer.client().mutate({
      operationName: 'smart/RemovePaymentDetails/v1',
      input: {
        baseSiteId,
        userId,
        paymentDetailsId,
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
