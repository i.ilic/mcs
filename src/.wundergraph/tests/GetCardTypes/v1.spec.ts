import type { OccGetCardTypesResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';

const mockResponse: OccGetCardTypesResponseData['occ_getCardTypes'] = {
  cardTypes: [{ code: 'visa', name: 'Visa' }],
};

const expectedResult = mockResponse.cardTypes;

describe('GetCardTypes', () => {
  it('should return the proper response', async () => {
    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path === `/${baseSiteId}/cardtypes?fields=DEFAULT`,
      handler: async () => ({
        status: 200,
        body: mockResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetCardTypes/v1',
      input: {
        baseSiteId,
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
