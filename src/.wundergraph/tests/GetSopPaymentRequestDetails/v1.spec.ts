import type { OccGetSopPaymentRequestDetailsResponseData } from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const cartId = '0000001';

describe('GetSopPaymentRequestDetails', () => {
  it('should return the proper response', async () => {
    const postUrl = 'foo';
    const mockResponse: OccGetSopPaymentRequestDetailsResponseData['occ_getSopPaymentRequestDetails'] =
      {
        postUrl,
      };

    const expectedResult = {
      ...mockResponse,
      mappingLabels: null,
      parameters: null,
      postUrl,
    };

    const scope = wgTs.mockServer.mock({
      match: ({ url, method }) =>
        method === 'GET' &&
        url.path ===
          `/${baseSiteId}/users/${userId}/carts/${cartId}/payment/sop/request?fields=DEFAULT&responseUrl=${postUrl}`,
      handler: async () => ({
        status: 200,
        body: mockResponse,
      }),
    });

    const result = await wgTs.testServer.client().query({
      operationName: 'smart/GetSopPaymentRequestDetails/v1',
      input: {
        baseSiteId,
        userId,
        cartId,
        responseUrl: postUrl,
      },
    });

    scope.done();

    expect(result.error).toBeUndefined();
    expect(result.data).toBeDefined();
    expect(result.data).toEqual(expectedResult);
  });
});
