import type { OccPlaceOrderResponseData } from '../../generated/models';
import { SmartResponseError } from '../../utils/errors/smart-response-error';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const cartId = '0000001';
const fields = 'FULL';

describe('PlaceOrder', () => {
  const mockResponse: OccPlaceOrderResponseData['occ_placeOrder'] = {
    code: '000000',
  };

  const expectedResult = {
    code: mockResponse.code,
    appliedCoupons: null,
    appliedOrderPromotions: null,
    appliedProductPromotions: null,
    appliedVouchers: null,
    calculated: null,
    cancellable: null,
    consignments: null,
    costCenter: null,
    created: null,
    deliveryAddress: null,
    deliveryCost: null,
    deliveryItemsQuantity: null,
    deliveryMode: null,
    deliveryOrderGroups: null,
    deliveryStatus: null,
    deliveryStatusDisplay: null,
    entries: null,
    entryGroups: null,
    guestCustomer: null,
    guid: null,
    net: null,
    orderDiscounts: null,
    orgCustomer: null,
    orgUnit: null,
    paymentAddress: null,
    paymentInfo: null,
    permissionResults: null,
    pickupItemsQuantity: null,
    pickupOrderGroups: null,
    productDiscounts: null,
    purchaseOrderNumber: null,
    returnable: null,
    site: null,
    status: null,
    statusDisplay: null,
    store: null,
    subTotal: null,
    totalDiscounts: null,
    totalItems: null,
    totalPrice: null,
    totalPriceWithTax: null,
    totalTax: null,
    totalUnitCount: null,
    unconsignedEntries: null,
    user: null,
  };

  describe('when terms are checked', () => {
    it('should return proper result', async () => {
      const termsChecked = true;
      const scope = wgTs.mockServer.mock({
        match: ({ url, method }) =>
          method === 'POST' &&
          url.path ===
            `/${baseSiteId}/users/${userId}/orders?cartId=${cartId}&fields=${fields}`,
        handler: async () => ({ status: 201, body: mockResponse }),
      });

      const result = await wgTs.testServer.client().mutate({
        operationName: 'smart/PlaceOrder/v1',
        input: {
          baseSiteId,
          userId,
          cartId,
          termsChecked,
          fields,
        },
      });

      scope.done();

      expect(result.error).toBeUndefined();
      expect(result.data).toBeDefined();
      expect(result.data).toEqual(expectedResult);
    });
  });

  describe('when terms are NOT checked', () => {
    it('should throw an error', async () => {
      const termsChecked = false;

      const result = await wgTs.testServer.client().mutate({
        operationName: 'smart/PlaceOrder/v1',
        input: {
          baseSiteId,
          userId,
          cartId,
          termsChecked,
          fields,
        },
      });

      expect(result.data).toBeUndefined();
      expect(result.error).toBeDefined();
      expect(result.error).toEqual(
        new SmartResponseError('Terms have not been checked')
      );
    });
  });
});
