import type {
  McsStorefrontCheckoutV1GetShippingAddressesResponseData,
  OccGetAddressesResponseData,
} from '../../generated/models';

const baseSiteId = 'electronics-spa';
const userId = 'current';
const checkoutId = '0000001';

describe('GetAddresses', () => {
  describe('get checkout shipping addresses', () => {
    const mockGetCheckoutShippingAddresses: McsStorefrontCheckoutV1GetShippingAddressesResponseData['mcsStorefrontCheckoutV1_getShippingAddresses'] =
      {
        pagination: {
          count: 0,
          page: 0,
          pageSize: 0,
        },
        results: [
          {
            firstName: 'foo firstName',
            lastName: 'foo lastName',
            addressLine1: 'foo line1',
            town: 'foo town',
            postalCode: 'foo postalCode',
            country: {
              code: 'foo country isoCode',
              name: 'foo country name',
            },
          },
        ],
      };

    describe('without region', () => {
      const expectedResult =
        mockGetCheckoutShippingAddresses.results?.map((address) => ({
          ...address,
          country: {
            isocode: address.country.code,
            name: address.country.name,
          },
          companyName: null,
          district: null,
          id: null,
          addressLine2: null,
          phone: null,
          title: null,
          titleCode: null,
        })) ?? [];

      it('should return the proper data', async () => {
        const checkoutShippingAddressScope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}/shippingAddresses`,
          handler: async () => ({
            status: 200,
            body: mockGetCheckoutShippingAddresses,
          }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetAddresses/v1',
          input: {
            baseSiteId,
            userId,
            checkoutId,
          },
        });

        checkoutShippingAddressScope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });

    describe('with region', () => {
      const mockGetCheckoutShippingAddressesWithRegion: McsStorefrontCheckoutV1GetShippingAddressesResponseData['mcsStorefrontCheckoutV1_getShippingAddresses'] =
        {
          pagination: {
            count: 0,
            page: 0,
            pageSize: 0,
          },
          results: mockGetCheckoutShippingAddresses.results?.map((address) => ({
            ...address,
            region: {
              code: 'foo region isoCode',
              codeShort: 'foo region isoCodeShort',
              country: 'foo region countryIso',
              name: 'foo region name',
            },
          })),
        };
      const expectedResult =
        mockGetCheckoutShippingAddressesWithRegion.results?.map((address) => ({
          ...address,
          country: {
            isocode: address.country.code,
            name: address.country.name,
          },
          region: {
            isocode: address.region?.code,
            isocodeShort: address.region?.codeShort,
            countryIso: address.region?.country,
            name: address.region?.name,
          },
          companyName: null,
          district: null,
          id: null,
          addressLine2: null,
          phone: null,
          title: null,
          titleCode: null,
        })) ?? [];

      it('should return the proper data', async () => {
        const checkoutShippingAddressScope = wgTs.mockServer.mock({
          match: ({ url, method }) =>
            method === 'GET' &&
            url.path ===
              `/checkout/v1/${baseSiteId}/users/${userId}/checkouts/${checkoutId}/shippingAddresses`,
          handler: async () => ({
            status: 200,
            body: mockGetCheckoutShippingAddressesWithRegion,
          }),
        });

        const result = await wgTs.testServer.client().query({
          operationName: 'smart/GetAddresses/v1',
          input: {
            baseSiteId,
            userId,
            checkoutId,
          },
        });

        checkoutShippingAddressScope.done();

        expect(result.error).toBeUndefined();
        expect(result.data).toBeDefined();
        expect(result.data).toEqual(expectedResult);
      });
    });
  });

  describe('get User addresses', () => {
    const mockUserAddresses: OccGetAddressesResponseData['occ_getAddresses'] = {
      addresses: [
        {
          firstName: 'foo firstName',
          lastName: 'foo lastName',
          line1: 'foo line1',
          town: 'foo town',
          postalCode: 'foo postalCode',
          country: {
            isocode: 'foo country isocode',
            name: 'foo country name',
          },
          region: {
            isocode: 'foo region isocode',
            isocodeShort: 'foo region isocodeShort',
            countryIso: 'foo region countryIso',
            name: 'foo region name',
          },
        },
      ],
    };
    const expectedResult =
      mockUserAddresses.addresses?.map((address) => ({
        ...address,
        cellphone: null,
        companyName: null,
        defaultAddress: null,
        district: null,
        email: null,
        formattedAddress: null,
        id: null,
        line2: null,
        phone: null,
        shippingAddress: null,
        title: null,
        titleCode: null,
        visibleInAddressBook: null,
      })) ?? [];

    it('should return the proper data', async () => {
      const userAddressScope = wgTs.mockServer.mock({
        match: ({ url, method }) =>
          method === 'GET' &&
          url.path ===
            `/${baseSiteId}/users/${userId}/addresses?fields=DEFAULT`,
        handler: async () => ({
          status: 200,
          body: mockUserAddresses,
        }),
      });

      const result = await wgTs.testServer.client().query({
        operationName: 'smart/GetAddresses/v1',
        input: {
          baseSiteId,
          userId,
        },
      });

      userAddressScope.done();

      expect(result.error).toBeUndefined();
      expect(result.data).toBeDefined();
      expect(result.data).toEqual(expectedResult);
    });
  });
});
