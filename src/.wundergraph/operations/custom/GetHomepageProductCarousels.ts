import { createOperation, z } from '../../generated/wundergraph.factory';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
  }),
  handler: async ({ context, input, operations }) => {
    // Get the homepage
    const homepage = await operations.query({
      operationName: 'occ/GetPage',
      input: {
        baseSiteId: input.baseSiteId,
        pageLabelOrId: 'homepage',
      },
    });

    // Merge additionalProperties on homepage data (API requirement)
    const homepageData = homepage.data!.occ_getPage!;
    Object.assign(homepageData, homepageData.additionalProperties);

    // Define the Section3 component interface according to our sample data
    interface Section3Component {
      title: string;
      productCodes: string;
    }

    // Define a function that gets minimal product data to display in carousel
    async function getProduct(productCode: string) {
      const { data } = await operations.query({
        operationName: 'internal/GetProductCarouselItem',
        input: {
          baseSiteId: input.baseSiteId,
          productCode,
        },
      });

      const { code, name, images } = data!.occ_getProduct!;

      const { altText, url } = images!.find((img) => img.imageType === 'PRIMARY' && img.format === 'product')!;

      return {
        code,
        name,
        img: {
          alt: altText || name,
        },
      };
    }

    // Define a function that gets minimal data for all products
    async function getProducts(component: Section3Component) {
      return Promise.all(component.productCodes!.split(/\s+/).map(getProduct));
    }

    // Get the two product carousel components in Section3
    const [bestSelling, newProduct] = homepageData.contentSlots!.contentSlot!.find((slot) => {
      return slot!.position === 'Section3';
    })!.components!.component as [Section3Component, Section3Component];

    // Get products for each carousel
    const bestSellingProducts = await getProducts(bestSelling);
    const newProductProducts = await getProducts(newProduct);

    // Return an array of carousels
    return [
      { title: bestSelling.title, products: bestSellingProducts },
      { title: newProduct.title, products: newProductProducts },
    ];
  },
});