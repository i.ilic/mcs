import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    fields: z.string().optional(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, fields } = input;

    const result = await operations.query({
      operationName: 'occ/GetCardTypes',
      input: {
        baseSiteId,
        fields,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_getCardTypes?.cardTypes ?? [];
  },
});
