import parse from 'node-html-parser';
import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.mutation({
  input: z.object({
    postUrl: z.string().url(),
    parameters: z.record(z.string(), z.string().or(z.boolean()).or(z.number())),
  }),
  handler: async ({ clientRequest, input }) => {
    const authorization = clientRequest.headers.get('Authorization') ?? '';

    const request = new Request(input.postUrl, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'text/html',
        Authorization: authorization,
      }),
      body: new URLSearchParams(input.parameters as Record<string, string>),
    });

    /**
     * Manually making request because the destination server does
     * not have to be a commerce instance.
     */
    const result = await fetch(request)
      .then((response) => response.text())
      .then((res) => extractPaymentDetailsFromHtml(res))
      .catch((error) => {
        throw error;
      });

    return result;
  },
});

function extractPaymentDetailsFromHtml(html: string): {
  [key: string]: string | boolean;
} {
  const document = parse(html);
  const responseForm = document.getElementsByTagName('form')[0];
  const inputs = responseForm.getElementsByTagName('input');

  const values: { [key: string]: string | boolean } = {};
  for (let i = 0; inputs[i]; i++) {
    const input = inputs[i];
    const name = input.getAttribute('name');
    const value = input.getAttribute('value');
    if (name && name !== '{}' && value && value !== '') {
      values[name] = value;
    }
  }

  return values;
}
