import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    fields: z.string().optional(),
    saved: z.boolean().optional(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, fields, saved } = input;

    const result = await operations.query({
      operationName: 'occ/GetPaymentDetailsList',
      input: {
        baseSiteId,
        userId,
        fields,
        saved,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_getPaymentDetailsList?.payments ?? [];
  },
});
