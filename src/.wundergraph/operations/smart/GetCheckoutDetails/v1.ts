import type { DeliveryMode, PaymentDetails } from '@spartacus/cart/base/root';
import type { CheckoutState } from '@spartacus/checkout/base/root';
import type { Address } from '@spartacus/core';
import type {
  McsStorefrontCheckoutV1GetCheckoutResponse,
  McsStorefrontCheckoutV1GetCheckoutResponseData,
  OccGetCartResponse,
  OccGetCartResponseData,
} from '../../../generated/models';
import { createOperation, z } from '../../../generated/wundergraph.factory';
import { convertCountry } from '../../../utils/converters/country-converter';
import { convertRegion } from '../../../utils/converters/region-converter';
import { formatPrice } from '../../../utils/price-formatter-utils';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    checkoutId: z.string(),
    locale: z.string(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, checkoutId, locale } = input;

    const checkoutDetailsPromise = operations.query({
      operationName: 'mcsStorefrontCheckoutV1/GetCheckout',
      input: {
        baseSiteId,
        userId,
        checkoutId,
      },
    });

    /**
     *
     * TODO: remove.
     *
     * only temporary until we get a new MCS instance
     * where we will receive "paymentInfo" property.
     *
     */
    const cartDetailsPromise = operations.query({
      operationName: 'occ/GetCart',
      input: {
        baseSiteId,
        userId,
        cartId: checkoutId,
        fields: 'paymentInfo(FULL)',
      },
    });

    const [checkoutDetails, cartDetails] = await Promise.all([
      checkoutDetailsPromise,
      cartDetailsPromise,
    ]);

    if (checkoutDetails.error) throw checkoutDetails.error;
    if (cartDetails.error) throw cartDetails.error;

    return createResponse(checkoutDetails, cartDetails, locale);
  },
});

function createResponse(
  checkoutDetails: McsStorefrontCheckoutV1GetCheckoutResponse,
  cartDetails: OccGetCartResponse,
  locale: string
) {
  const { mcsStorefrontCheckoutV1_getCheckout } = checkoutDetails.data ?? {};
  const { occ_getCart } = cartDetails.data ?? {};

  const deliveryAddress = createDeliveryAddress(
    mcsStorefrontCheckoutV1_getCheckout
  );
  const deliveryMode = createDeliveryMode(
    mcsStorefrontCheckoutV1_getCheckout,
    locale
  );
  const paymentInfo = createPaymentInfo(occ_getCart);

  const response: CheckoutState = {
    ...(deliveryAddress && { deliveryAddress }),
    ...(deliveryMode && { deliveryMode }),
    ...(paymentInfo && { paymentInfo }),
  };

  return response;
}

function createDeliveryAddress(
  mcsStorefrontCheckoutV1_getCheckout: McsStorefrontCheckoutV1GetCheckoutResponseData['mcsStorefrontCheckoutV1_getCheckout']
) {
  if (!mcsStorefrontCheckoutV1_getCheckout?.shippingAddress) return;

  const deliveryAddress: Address = {
    ...mcsStorefrontCheckoutV1_getCheckout.shippingAddress,
    region: convertRegion(
      mcsStorefrontCheckoutV1_getCheckout.shippingAddress.region
    ),
    country: convertCountry(
      mcsStorefrontCheckoutV1_getCheckout.shippingAddress.country
    ),
    line1: mcsStorefrontCheckoutV1_getCheckout.shippingAddress.addressLine1,
    line2: mcsStorefrontCheckoutV1_getCheckout.shippingAddress.addressLine2,
  };

  return deliveryAddress;
}

function createDeliveryMode(
  mcsStorefrontCheckoutV1_getCheckout: McsStorefrontCheckoutV1GetCheckoutResponseData['mcsStorefrontCheckoutV1_getCheckout'],
  locale: string
) {
  if (!mcsStorefrontCheckoutV1_getCheckout?.deliveryOption) return;

  const deliveryMode: DeliveryMode = {
    ...mcsStorefrontCheckoutV1_getCheckout.deliveryOption,
    deliveryCost: {
      formattedValue: formatPrice({
        locale,
        currency: mcsStorefrontCheckoutV1_getCheckout.currency?.code,
        price:
          mcsStorefrontCheckoutV1_getCheckout?.deliveryOption?.deliveryCost ??
          0,
      }),
    },
  };

  return deliveryMode;
}

function createPaymentInfo(occ_getCart: OccGetCartResponseData['occ_getCart']) {
  if (!occ_getCart?.paymentInfo) return;

  const paymentInfo: PaymentDetails = occ_getCart.paymentInfo;

  return paymentInfo;
}
