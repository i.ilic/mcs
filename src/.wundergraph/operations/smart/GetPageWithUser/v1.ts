import {
  OccGetPageByIdAndUserInput,
  OccGetPageWithUserInput,
} from '../../../generated/models';
import { createOperation, z } from '../../../generated/wundergraph.factory';
import { convertCmsPageResponse } from '../../../utils/converters/cms-page-converter';
import { fields } from '../../../utils/validation-utils';

const operationInput = z.object({
  baseSiteId: z.string(),
  userId: z.string(),
  fields: fields.optional(),
  pageContext: z.object({
    id: z.string(),
    type: z
      .union([
        z.literal('CatalogPage'),
        z.literal('CategoryPage'),
        z.literal('ContentPage'),
        z.literal('ProductPage'),
      ])
      .optional(),
  }),
  routePageContextId: z.string(),
  cmsTicketId: z.string().optional(),
});

export default createOperation.query({
  input: operationInput,
  handler: async ({ input, operations }) => {
    const { pageContext } = input;

    if (!pageContext.type) {
      const result = await operations.query({
        operationName: 'occ/GetPageByIdAndUser',
        input: createGetPageByIdInput(input),
      });

      if (result.error) throw result.error;

      return convertCmsPageResponse(result.data?.occ_getPageByIdAndUser);
    }

    const result = await operations.query({
      operationName: 'occ/GetPageWithUser',
      input: createGetPageQueryParams(input),
    });

    if (result.error) throw result.error;

    return convertCmsPageResponse(result.data?.occ_getPageWithUser);
  },
});

function createGetPageByIdInput({
  baseSiteId,
  pageContext,
  userId,
  cmsTicketId,
  fields,
  routePageContextId,
}: z.infer<typeof operationInput>) {
  const input: OccGetPageByIdAndUserInput = {
    baseSiteId,
    userId,
    fields,
    cmsTicketId,
    pageId: pageContext.id,
  };

  if (pageContext.id === 'productList') {
    input.categoryCode = routePageContextId;
  }

  return input;
}

function createGetPageQueryParams({
  baseSiteId,
  pageContext,
  userId,
  cmsTicketId,
  fields,
}: z.infer<typeof operationInput>) {
  const input: OccGetPageWithUserInput = {
    baseSiteId,
    userId,
    fields,
    cmsTicketId,
  };

  if (
    pageContext.id === '__HOMEPAGE__' ||
    pageContext.id === 'smartedit-preview'
  ) {
    return input;
  }

  if (pageContext.type) {
    input.pageType = pageContext.type;
  }

  if (pageContext.type === 'ContentPage') {
    input.pageLabelOrId = pageContext.id;
  } else {
    input.code = pageContext.id;
  }

  return input;
}
