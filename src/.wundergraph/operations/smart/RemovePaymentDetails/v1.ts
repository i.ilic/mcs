import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.mutation({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    paymentDetailsId: z.string(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, paymentDetailsId } = input;

    const result = await operations.mutate({
      operationName: 'occ/RemovePaymentDetails',
      input: {
        baseSiteId,
        userId,
        paymentDetailsId,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_removePaymentDetails ?? {};
  },
});
