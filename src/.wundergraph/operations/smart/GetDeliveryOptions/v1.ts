import type { Occ } from '@spartacus/core';
import type {
  McsStorefrontCheckoutV1GetCheckoutResponse,
  McsStorefrontCheckoutV1GetDeliveryOptionsResponse,
} from '../../../generated/models';
import { createOperation, z } from '../../../generated/wundergraph.factory';
import { formatPrice } from '../../../utils/price-formatter-utils';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    checkoutId: z.string(),
    locale: z.string(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, checkoutId, locale } = input;

    const checkoutDetailsPromise = operations.query({
      operationName: 'mcsStorefrontCheckoutV1/GetCheckout',
      input: {
        baseSiteId,
        userId,
        checkoutId,
      },
    });
    const deliveryOptionsPromise = operations.query({
      operationName: 'mcsStorefrontCheckoutV1/GetDeliveryOptions',
      input: {
        baseSiteId,
        userId,
        checkoutId,
      },
    });

    const [checkoutDetails, deliveryOptions] = await Promise.all([
      checkoutDetailsPromise,
      deliveryOptionsPromise,
    ]);

    if (checkoutDetails.error) throw checkoutDetails.error;
    if (deliveryOptions.error) throw deliveryOptions.error;

    return createResponse({
      deliveryOptions,
      checkoutDetails,
      locale,
    });
  },
});

function createResponse(parameters: {
  deliveryOptions: McsStorefrontCheckoutV1GetDeliveryOptionsResponse;
  checkoutDetails: McsStorefrontCheckoutV1GetCheckoutResponse;
  locale: string;
}) {
  const { deliveryOptions, checkoutDetails, locale } = parameters;

  const currency =
    checkoutDetails.data?.mcsStorefrontCheckoutV1_getCheckout?.currency?.code;

  const deliveryOptionsResponse =
    deliveryOptions.data?.mcsStorefrontCheckoutV1_getDeliveryOptions?.results ??
    [];

  const response: Occ.DeliveryMode[] = deliveryOptionsResponse.map(
    (deliveryOption) => ({
      ...deliveryOption,
      deliveryCost: {
        formattedValue: formatPrice({
          locale,
          currency,
          price: deliveryOption.deliveryCost ?? 0,
        }),
      },
    })
  );

  return response;
}
