import type {
  McsStorefrontCheckoutV1GetShippingAddressesResponse,
  OccGetAddressesResponse,
} from '../../../generated/models';
import { createOperation, z } from '../../../generated/wundergraph.factory';
import { convertCountry } from '../../../utils/converters/country-converter';
import { convertRegion } from '../../../utils/converters/region-converter';

/**
 * This operation handles loading the addresses for
 * both User address book page and Checkout shipping address.
 */
export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    checkoutId: z.string().optional(),
  }),
  handler: async ({ input, operations }) => {
    const { checkoutId, baseSiteId, userId } = input;

    if (isCheckoutShippingAddress(checkoutId)) {
      type ShippingAddressRequiredInput = Required<typeof input>;

      const result = await operations.query({
        operationName: 'mcsStorefrontCheckoutV1/GetShippingAddresses',
        input: input as ShippingAddressRequiredInput,
      });

      if (result.error) throw result.error;

      return createCheckoutShippingAddressResult(result);
    }

    const result = await operations.query({
      operationName: 'occ/GetAddresses',
      input: {
        baseSiteId,
        userId,
      },
    });

    if (result.error) throw result.error;

    return createUserAddressResult(result);
  },
});

function isCheckoutShippingAddress(checkoutId: string | undefined) {
  return !!checkoutId;
}

function createCheckoutShippingAddressResult(
  response: McsStorefrontCheckoutV1GetShippingAddressesResponse
) {
  const { mcsStorefrontCheckoutV1_getShippingAddresses } = response.data ?? {};
  const addresses = mcsStorefrontCheckoutV1_getShippingAddresses?.results ?? [];

  return addresses?.map((address) => ({
    ...address,
    region: convertRegion(address.region),
    country: convertCountry(address.country),
  }));
}

function createUserAddressResult(response: OccGetAddressesResponse) {
  const { occ_getAddresses } = response.data ?? {};
  return occ_getAddresses?.addresses ?? [];
}
