import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.mutation({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    cartId: z.string(),
    paymentDetailsId: z.string(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, cartId, paymentDetailsId } = input;

    const result = await operations.mutate({
      operationName: 'occ/ReplaceCartPaymentDetails',
      input: {
        baseSiteId,
        userId,
        cartId,
        paymentDetailsId,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_replaceCartPaymentDetails;
  },
});
