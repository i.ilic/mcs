import { createOperation, z } from '../../../generated/wundergraph.factory';

const countrySchema = z.object({
  isocode: z.string().optional(),
  name: z.string().optional(),
});

const regionSchema = z.object({
  countryIso: z.string().optional(),
  isocode: z.string().optional(),
  isocodeShort: z.string().optional(),
  name: z.string().optional(),
});

const addressSchema = z.object({
  cellphone: z.string().optional(),
  companyName: z.string().optional(),
  country: countrySchema.optional(),
  defaultAddress: z.boolean().optional(),
  district: z.string().optional(),
  email: z.string().optional(),
  firstName: z.string().optional(),
  formattedAddress: z.string().optional(),
  id: z.string().optional(),
  lastName: z.string().optional(),
  line1: z.string().optional(),
  line2: z.string().optional(),
  phone: z.string().optional(),
  postalCode: z.string().optional(),
  region: regionSchema.optional(),
  shippingAddress: z.boolean().optional(),
  title: z.string().optional(),
  titleCode: z.string().optional(),
  town: z.string().optional(),
  visibleInAddressBook: z.boolean().optional(),
});

const cardTypesSchema = z.object({
  code: z.string().optional(),
  name: z.string().optional(),
});

const inputSchema = z.object({
  accountHolderName: z.string().optional(),
  billingAddress: addressSchema.optional(),
  cardNumber: z.string().optional(),
  cardType: cardTypesSchema.optional(),
  defaultPayment: z.boolean().optional(),
  expiryMonth: z.string().optional(),
  expiryYear: z.string().optional(),
  id: z.string().optional(),
  issueNumber: z.string().optional(),
  saved: z.boolean().optional(),
  startMonth: z.string().optional(),
  startYear: z.string().optional(),
  subscriptionId: z.string().optional(),
});

export default createOperation.mutation({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    paymentDetailsId: z.string(),
    input: inputSchema.optional(),
  }),
  handler: async ({ input, operations }) => {
    const {
      baseSiteId,
      userId,
      paymentDetailsId,
      input: paymentDetailsInput,
    } = input;

    const result = await operations.mutate({
      operationName: 'occ/UpdatePaymentDetails',
      input: {
        baseSiteId,
        userId,
        paymentDetailsId,
        input: paymentDetailsInput,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_updatePaymentDetails ?? {};
  },
});
