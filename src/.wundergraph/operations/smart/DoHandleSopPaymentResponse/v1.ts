import type { PaymentDetails } from '@spartacus/cart/base/root';
import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.mutation({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    cartId: z.string(),
    parameters: z.record(z.string(), z.string().or(z.boolean())),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, cartId, parameters, userId } = input;

    const { data, error } = await operations.mutate({
      operationName: 'occ/DoHandleSopPaymentResponse',
      input: {
        baseSiteId,
        cartId,
        userId,
        input: parameters,
      },
    });

    if (error) {
      throw error;
    }

    const response: PaymentDetails | undefined =
      data?.occ_doHandleSopPaymentResponse;

    return response;
  },
});
