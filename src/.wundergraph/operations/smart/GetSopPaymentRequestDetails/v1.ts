import { createOperation, z } from '../../../generated/wundergraph.factory';

export default createOperation.query({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    cartId: z.string(),
    responseUrl: z.string(),
  }),
  handler: async ({ input, operations }) => {
    const { baseSiteId, userId, cartId, responseUrl } = input;

    const result = await operations.query({
      operationName: 'occ/GetSopPaymentRequestDetails',
      input: {
        baseSiteId,
        userId,
        cartId,
        responseUrl,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_getSopPaymentRequestDetails;
  },
});
