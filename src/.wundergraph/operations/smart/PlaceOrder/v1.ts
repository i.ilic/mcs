import { createOperation, z } from '../../../generated/wundergraph.factory';
import { SmartResponseError } from '../../../utils/errors/smart-response-error';

export default createOperation.mutation({
  input: z.object({
    baseSiteId: z.string(),
    userId: z.string(),
    cartId: z.string(),
    termsChecked: z.boolean(),
    fields: z.string().optional(),
  }),
  handler: async ({ input, operations }) => {
    const { termsChecked, baseSiteId, userId, cartId, fields } = input;

    if (!termsChecked)
      throw new SmartResponseError('Terms have not been checked');

    /**
     *  TODO: this is out of scope for the Pilot.
     * if (input.userId === 'anonymous') {
     *   1. fetch the client token
     *   2. use it instead of the User token
     * }
     */

    const result = await operations.mutate({
      operationName: 'occ/PlaceOrder',
      input: {
        baseSiteId,
        userId,
        cartId,
        fields,
      },
    });

    if (result.error) throw result.error;

    return result.data?.occ_placeOrder ?? {};
  },
});
