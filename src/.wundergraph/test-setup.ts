import { createTestAndMockServer } from './generated/testing';

const _wgTs = createTestAndMockServer({
  env: {
    WG_DISABLE_EMBEDDED_NATS: 'true',
  },
});

beforeAll(
  () =>
    _wgTs.start({
      mockURLEnvs: [
        'STOREFRONT_URL',
        'OCC_URL',
        'OCC_SCHEMA_URL',
        'AUTH_URL',
        'ALLOWED_ORIGIN',
        'API_DISCOVERY_URL',
        'API_DISCOVERY_CLIENT_ID',
        'API_DISCOVERY_CLIENT_SECRET',
        'API_DISCOVERY_GRANT_TYPE',
      ],
      timeout: 180000,
    }),
  180000
);

afterAll(() => _wgTs.stop());

(global as any).wgTs = _wgTs;
declare global {
  const wgTs: typeof _wgTs;
}
