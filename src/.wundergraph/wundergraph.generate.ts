import { configureWunderGraphGeneration } from '@wundergraph/sdk';
import { AngularTemplate } from './angular-template';

export default configureWunderGraphGeneration({
  codeGenerators: [
    {
      templates: [new AngularTemplate()],
      path: '../client',
    },
  ],
  operationsGenerator: (config) => {
    config.excludeNamespaces();
  },
});
