/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { NgModule, inject } from '@angular/core';
import { provideBFFAdapters, provideClientToBFFAdapters } from '@spartacus/bff';
import {
  CLIENT_AUTH_OPERATIONS_CONFIG,
  ClientAuthTokenInterceptor,
  ErrorInterceptor,
  RemoveSdkVersionInterceptor,
  UserAuthTokenInterceptor,
  defaultClientAuthOperationsConfig,
} from '@spartacus/bff/root';
import { provideConfigFactory } from '@spartacus/core';
import {
  provideFetchInterceptors,
  provideWunderGraphFactory,
} from '../../client/angular';
import environment from '../../environments/environment';
import { ENVIRONMENT, provideEnvironment } from './environment-provider';
import { OutletPosition, provideOutlet } from '@spartacus/storefront';
import { HomepageProductCarouselsComponent } from './homepage-product-carousels.component';

@NgModule({
  providers: [
    provideEnvironment(environment),
    provideConfigFactory(() => {
      const env = inject(ENVIRONMENT);

      return {
        backend: {
          occ: {
            baseUrl: env.OCC_BASE_URL,
          },
        },
      };
    }),
    provideWunderGraphFactory(() => {
      const env = inject(ENVIRONMENT);

      return { baseURL: env.BFF_BASE_URL };
    }),
    {
      provide: CLIENT_AUTH_OPERATIONS_CONFIG,
      useValue: defaultClientAuthOperationsConfig,
    },
    provideFetchInterceptors([
      ClientAuthTokenInterceptor,
      UserAuthTokenInterceptor,
      RemoveSdkVersionInterceptor,
      // the error interceptor should be the last provided interceptor
      ErrorInterceptor,
    ]),
    provideClientToBFFAdapters(),
    provideBFFAdapters({}),
    provideOutlet({
      id: 'Section3',
      position: OutletPosition.REPLACE,
      component: HomepageProductCarouselsComponent,
    }),
  ],
})
export class SpartacusCustomizationModule {}
