/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { InjectionToken, inject } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';

const key = makeStateKey<Environment>('__ENV__');

export const provideEnvironment = (environment?: Environment) => [
  {
    provide: ENVIRONMENT,
    useFactory: () => {
      const state = inject(TransferState);
      const serverEnv = inject(SERVER_ENVIRONMENT, { optional: true });
      const env = Object.assign({}, environment, serverEnv);

      if (!state.hasKey(key)) state.set(key, env);

      return state.get(key, env);
    },
  },
];

export const ENVIRONMENT = new InjectionToken<Environment>('ENVIRONMENT');
export const SERVER_ENVIRONMENT = new InjectionToken<Environment>(
  'SERVER_ENVIRONMENT'
);

export interface Environment {
  BFF_BASE_URL?: string;
  OCC_BASE_URL?: string;
}
