import { TestBed } from '@angular/core/testing';
import { provideBFFAdapters } from '@spartacus/bff';
import {
  ErrorInterceptor,
  RemoveSdkVersionInterceptor,
} from '@spartacus/bff/root';
import { Config } from '@spartacus/core';
import {
  AngularWunderGraphClient,
  WUNDERGRAPH,
  provideFetchInterceptors,
} from '../../client/angular';
import { SERVER_ENVIRONMENT } from './environment-provider';
import { SpartacusCustomizationModule } from './spartacus-customization.module';

jest.mock('@spartacus/bff', () => ({
  ...jest.requireActual('@spartacus/bff'),
  provideBFFAdapters: jest.fn(() => []),
}));

jest.mock('../../client/angular', () => ({
  ...jest.requireActual('../../client/angular'),
  provideFetchInterceptors: jest.fn(() => []),
}));

describe('SpartacusCustomizationModule', () => {
  let config: Config;
  let client: AngularWunderGraphClient;

  beforeAll(async () => {
    await TestBed.configureTestingModule({
      imports: [SpartacusCustomizationModule],
      providers: [
        {
          provide: SERVER_ENVIRONMENT,
          useValue: {
            BFF_BASE_URL: 'https://bff.base.url',
            OCC_BASE_URL: 'https://occ.base.url',
          },
        },
      ],
    }).compileComponents();

    config = TestBed.inject(Config);
    client = TestBed.inject(WUNDERGRAPH);
  });

  it('should provide OCC baseURL config from environment', () => {
    expect(config.backend?.occ?.baseUrl).toBe('https://occ.base.url');
  });

  it('should provide the client to the app', () => {
    expect(client).toBeInstanceOf(AngularWunderGraphClient);
  });

  it('should provide fetch interceptors', () => {
    expect(provideFetchInterceptors).toHaveBeenCalledWith(
      expect.arrayContaining([RemoveSdkVersionInterceptor, ErrorInterceptor])
    );
  });

  it('should provide BFF adapters for OCC and MCS', () => {
    expect(provideBFFAdapters).toHaveBeenCalled();
  });
});
