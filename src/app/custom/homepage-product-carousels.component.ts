import { AsyncPipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { BaseSiteService } from '@spartacus/core';
import { map, switchMap } from 'rxjs/operators';
import { WUNDERGRAPH } from '../../client/angular';

@Component({
  imports: [AsyncPipe, JsonPipe],
  standalone: true,
  template: `<pre><code>{{ productCarousels$ | async | json }}</code></pre>`,
  styles: [
    `
      pre {
        background: ghostwhite;
        padding: 1em;
        max-width: min(100vw, var(--cx-page-width-max));
        overflow-x: scroll;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomepageProductCarouselsComponent {
  wundergraph = inject(WUNDERGRAPH);

  baseSite$ = inject(BaseSiteService).getActive();

  mapToProductCarousels = (baseSiteId: string) =>
    this.wundergraph
      .query$({
        operationName: 'custom/GetHomepageProductCarousels',
        input: { baseSiteId },
      })
      .pipe(map((result) => result.data));

  productCarousels$ = this.baseSite$.pipe(switchMap(this.mapToProductCarousels));
}
