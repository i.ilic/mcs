/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { NgModule } from '@angular/core';
import { BffOrderAdapter } from '@spartacus/bff/occ/order';
import { OrderModule } from '@spartacus/order';
import { OrderAdapter } from '@spartacus/order/core';

@NgModule({
  imports: [OrderModule],
  providers: [
    {
      provide: OrderAdapter,
      useClass: BffOrderAdapter,
    },
  ],
})
export class OrderWrapperModule {}
