/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { NgModule } from '@angular/core';
import { OCC_CART_ADAPTERS } from '@spartacus/bff/occ/cart';
import { CartBaseModule } from '@spartacus/cart/base';

@NgModule({
  imports: [CartBaseModule],
  providers: [OCC_CART_ADAPTERS],
})
export class CartBaseWrapperModule {}
