/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { NgModule } from '@angular/core';
import {
  BffCheckoutDeliveryOptionAdapter,
  BffCheckoutDetailsAdapter,
  BffCheckoutPaymentAdapter,
  BffCheckoutShippingAddressAdapter,
} from '@spartacus/bff/mcs/checkout';
import { CheckoutModule } from '@spartacus/checkout/base';
import {
  CheckoutAdapter,
  CheckoutDeliveryAddressAdapter,
  CheckoutDeliveryModesAdapter,
  CheckoutPaymentAdapter,
} from '@spartacus/checkout/base/core';

@NgModule({
  imports: [CheckoutModule],
  providers: [
    {
      provide: CheckoutAdapter,
      useExisting: BffCheckoutDetailsAdapter,
    },
    {
      provide: CheckoutDeliveryAddressAdapter,
      useExisting: BffCheckoutShippingAddressAdapter,
    },
    {
      provide: CheckoutDeliveryModesAdapter,
      useExisting: BffCheckoutDeliveryOptionAdapter,
    },
    {
      provide: CheckoutPaymentAdapter,
      useExisting: BffCheckoutPaymentAdapter,
    },
  ],
})
export class CheckoutWrapperModule {}
