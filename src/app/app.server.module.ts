import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { provideServer } from '@spartacus/setup/ssr';
import { SERVER_ENVIRONMENT } from './custom/environment-provider';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    ...provideServer({
        serverRequestOrigin: process.env['SERVER_REQUEST_ORIGIN'],
    }),
    {
        provide: SERVER_ENVIRONMENT,
        useValue: {
            BFF_BASE_URL: process.env['BFF_BASE_URL'],
            OCC_BASE_URL: process.env['OCC_BASE_URL']
        }
    }
],
})
export class AppServerModule {}
