import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { render } from '@testing-library/angular';
import { AppComponent } from './app.component';

@Component({
  selector: 'cx-storefront',
  template: `<div>Spartacus</div>`,
})
class MockCxStorefrontComponent {}

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should render Spartacus properly', async () => {
    const renderResult = await render(AppComponent, {
      declarations: [MockCxStorefrontComponent],
    });

    const div = renderResult.getByText(/Spartacus/i, { selector: 'div' });
    expect(div).toBeTruthy();
  });
});
