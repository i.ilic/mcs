import { expect } from '@jest/globals';
import { diff } from 'jest-diff';

expect.extend({
  toHaveWunderGraphQueryParameters(
    requestOrGetter: Request | (() => Request),
    expected: object
  ) {
    const { url } =
      typeof requestOrGetter === 'function'
        ? requestOrGetter()
        : requestOrGetter;
    const wgQueryParams = new URLSearchParams(url).get('wg_variables') ?? '';

    const pass: boolean = this.equals(JSON.parse(wgQueryParams), expected);

    return {
      pass,
      message: () => diff(expected, JSON.parse(String(wgQueryParams))) || '',
    };
  },
});

interface CustomMatchers<R = unknown> {
  /**
   * Looks for the WunderGraph's query parameter in the given request,
   * and checks it against the provided object.
   */
  toHaveWunderGraphQueryParameters(expected: object): R;
}

declare global {
  namespace jest {
    interface Expect extends CustomMatchers {}
    interface Matchers<R> extends CustomMatchers<R> {}
    interface InverseAsymmetricMatchers extends CustomMatchers {}
  }
}
