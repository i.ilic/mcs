import JSDOMEnvironment from 'jest-environment-jsdom';

export default class extends JSDOMEnvironment {
  constructor(...args: ConstructorParameters<typeof JSDOMEnvironment>) {
    super(...args);

    (this.global as any).fetch = fetch;
    (this.global as any).AbortController = AbortController;
    (this.global as any).Headers = Headers;
    (this.global as any).Request = Request;
    (this.global as any).Response = Response;
    (this.global as any).TextDecoder = TextDecoder;
    (this.global as any).TextEncoder = TextEncoder;
  }
}
