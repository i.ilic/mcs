import * as _msw from 'msw';
import { setupServer } from 'msw/node';

const _wait = (ms = 0) => new Promise((res) => setTimeout(res, ms));
(global as any).wait = _wait;

class MswUtils {
  server = setupServer();

  #rest: RequestHandler = (method, url, bodyOrResponseOrResolver, options) => {
    const respond = createResponseFn(bodyOrResponseOrResolver);

    let request!: Request;

    msw.server.use(
      msw.http[method](
        url,
        async (info) => {
          request = info.request;

          return respond(info);
        },
        options
      )
    );

    return [() => request];
  };

  all = (...params: HandlerParams) => this.#rest('all', ...params);
  delete = (...params: HandlerParams) => this.#rest('delete', ...params);
  get = (...params: HandlerParams) => this.#rest('get', ...params);
  head = (...params: HandlerParams) => this.#rest('head', ...params);
  options = (...params: HandlerParams) => this.#rest('options', ...params);
  patch = (...params: HandlerParams) => this.#rest('patch', ...params);
  post = (...params: HandlerParams) => this.#rest('post', ...params);
  put = (...params: HandlerParams) => this.#rest('put', ...params);
}

(global as any).msw = {
  ..._msw,
  ...new MswUtils(),
};

beforeAll(() => {
  msw.server.listen({
    onUnhandledRequest: ({ url }, print) => {
      if (url.includes('127.0.0.1')) return;

      print.error();
    },
  });
});

beforeEach(() => {
  msw.server.resetHandlers();
});

afterAll(() => {
  msw.server.close();
});

declare global {
  const msw: typeof _msw & MswUtils;
  const wait: typeof _wait;
}

type HandlerParams = [
  url: string,
  bodyOrResponseOrResolver: object | string | Response | _msw.ResponseResolver,
  options?: _msw.RequestHandlerOptions
];

type RequestHandler = (
  method: keyof typeof msw.http,
  ...params: HandlerParams
) => [requestGetter: () => Request];

function createResponseFn(bodyOrResponseOrResolver: HandlerParams[1]) {
  if (typeof bodyOrResponseOrResolver === 'function')
    return bodyOrResponseOrResolver;

  if (bodyOrResponseOrResolver instanceof Response)
    return () => bodyOrResponseOrResolver;

  if (typeof bodyOrResponseOrResolver === 'string')
    return () => _msw.HttpResponse.text(bodyOrResponseOrResolver);

  return () => _msw.HttpResponse.json(bodyOrResponseOrResolver);
}

export default {};
