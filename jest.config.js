/** @type {import("jest").Config} */
module.exports = {
  preset: "jest-preset-angular",
  testEnvironment: "<rootDir>/jest.environment.ts",
  testEnvironmentOptions: {
    customExportConditions: [""],
  },
  setupFilesAfterEnv: ["<rootDir>/test-setup.ts"],
  globalSetup: "jest-preset-angular/global-setup",
  testPathIgnorePatterns: ["/node_modules/", "/.wundergraph/"],
  coverageReporters: ["html"],
  collectCoverageFrom: ["<rootDir>/src/app/**/*.ts"],
  coveragePathIgnorePatterns: [
    "<rootDir>/src/app/spartacus",
    "<rootDir>/src/app/app.module.ts",
    "<rootDir>/src/app/app.server.module.ts",
  ],
  coverageDirectory: "<rootDir>/coverage/angular",
};
