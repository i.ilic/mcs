/*
 * SPDX-FileCopyrightText: 2023 SAP Spartacus team <spartacus-team@sap.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

const {
  copyFileSync,
  lstatSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  writeFileSync,
} = require('fs');
const { dirname, join } = require('path');

const OUT_DIR = '.vercel/output';
const FUNCTIONS_DIR = `${OUT_DIR}/functions`;
const STATIC_DIR = `${OUT_DIR}/static`;
const PROJECT_DIST = 'dist/storefront';
const SSG_ROUTES_FILE_PATH = 'routes-ssg.txt';
const ISR_ROUTES_FILE_PATH = 'routes-isr.txt';
const SITE_CONTEXT_PATTERN = /^\/[^/]+\/[^/]+\/[^/]+/;
const ISR_REVALIDATION_INTERVAL_IN_SECONDS = 60 * 60; // 1 hour

// Array of routes to prerender
// Updates to the routes-ssg.txt file require a deployment
const SSG_PAGES = parseTextFile(SSG_ROUTES_FILE_PATH).map((route) => ({
  route,
  staticHTML: join(route.replace(SITE_CONTEXT_PATTERN, ''), 'index.html'),
}));

// Array of routes with Incremental Static Regeneration (ISR)
// Updates to the routes-isr.txt file require a deployment
// Prerendered files will be used as fallback for the initial render
const ISR_PAGES = parseTextFile(ISR_ROUTES_FILE_PATH).map((route) => ({
  name: `isr${createFunctionName(route)}`,
  route,
  fallback: join('../static', route, 'index.html'),
}));

// Create the static folder & copy browser files to it
mkdirSync(STATIC_DIR, { recursive: true });
copyFiles(`${PROJECT_DIST}/browser`, STATIC_DIR);

// Create an ISR serverless function for each ISR page
ISR_PAGES.forEach(createISRFunction);

// Create an SSR serverless function for all pages that are not ISR or SSG
createSSRFunction();

// Create the main config file
write(
  `${OUT_DIR}/config.json`,
  JSON.stringify({
    version: 1,
    routes: [
      ...SSG_PAGES.map((page) => ({
        src: page.route,
        dest: page.staticHTML,
      })),
      ...ISR_PAGES.map((page) => ({
        src: page.route,
        dest: `/${page.name}?__pathname=${page.route}`,
      })),
      // Specify that SSR should be used for all other pages
      {
        src: '/.*',
        dest: '/ssr',
      },
    ],
  })
);

// Creates the SSR function
function createSSRFunction() {
  const funcDir = `${FUNCTIONS_DIR}/ssr.func`;

  createFunction(funcDir);
}

// Creates an ISR function
function createISRFunction({ name, fallback }, index) {
  const funcPath = `${FUNCTIONS_DIR}/${name}`;
  const funcDir = `${funcPath}.func`;

  createFunction(funcDir);

  // Create prerender config json file
  write(
    `${funcPath}.prerender-config.json`,
    JSON.stringify({
      expiration: ISR_REVALIDATION_INTERVAL_IN_SECONDS,
      group: index + 1,
      allowQuery: ['__pathname'],
      passQuery: true,
      fallback,
    })
  );
}

// Creates a function directory
function createFunction(funcDir) {
  // Add config.json
  write(
    `${funcDir}/.vc-config.json`,
    JSON.stringify({
      launcherType: 'Nodejs',
      runtime: 'nodejs18.x',
      handler: 'index.js',
    })
  );

  // Copy server files
  copyFiles(`${PROJECT_DIST}/server`, funcDir);

  // Copy static files
  mkdirSync(`${funcDir}/${PROJECT_DIST}/browser`, { recursive: true });
  copyFiles(`${PROJECT_DIST}/browser`, `${funcDir}/${PROJECT_DIST}/browser`);

  // Create an entry file
  write(`${funcDir}/index.js`, `module.exports = require("./main.js").app();`);
}

// Writes content to a file
function write(file, data) {
  try {
    mkdirSync(dirname(file), { recursive: true });
  } catch {}

  writeFileSync(file, data);
}

// Copies files from one directory to another
function copyFiles(source, target) {
  const files = readdirSync(source);
  for (const file of files) {
    const curSource = `${source}/${file}`;
    if (lstatSync(curSource).isDirectory()) {
      mkdirSync(`${target}/${file}`, { recursive: true });
      copyFiles(curSource, `${target}/${file}`);
    } else {
      copyFileSync(curSource, `${target}/${file}`);
    }
  }
}

// Parses a TXT file
function parseTextFile(filepath) {
  return readFileSync(filepath, 'utf8').split(/\r?\n/).filter(Boolean);
}

// Creates a name by converting paths to kebab case
function createFunctionName(route) {
  return route.toLowerCase().replace(/^\/$/, '-root').replaceAll('/', '-');
}
